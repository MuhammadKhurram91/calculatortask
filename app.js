var express = require('express');
var app = express();
var routes = require('./routes');

app.set('view engine','ejs');
app.locals.pagetitle='WOW';



app.get('/',routes.index);
app.get('/about',routes.index);

app.get('*', function(req, res) {
    res.send('Bad Routes');
});

var sever = app.listen(3000, function(){
    console.log('listen on port 3000');
});